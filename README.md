Ситимобил
Тестовое задание на разработку мобильного приложения “движение автомобиля”.

Задача. Требуется разработать простое приложение, состоящее из одного представления
(view), являющего собой условный вид сверху на плоскость, на которой располагается объект
«автомобиль».
При нажатии (tap) на какое-либо место плоскости, автомобиль должен осуществлять
движение к этому месту. В моменты когда автомобиль находится в движении, нажатия на
экран могут игнорироваться.
Задача подразумевает, что плоскость не ограничивается границами представления (экрана),
то есть при движении автомобиль может частично или полностью скрываться из вида, но не
может остановиться за границами представления.
Возможны два способа решения: простое «A» и более реалистичное «B».

Решение «А»: линейное
Перед началом движения автомобиль разворачивается на месте (как танк), вокруг своей оси,
затем перемещается в точку, соответствующую месту нажатия на экран.

Решение «B»: реалистичное
Автомобиль движется к точке, соответствующей месту нажатия на экран, совершая при
движении поворот по дуге, подобно тому, как это делает настоящая машина. То есть: по дуге,
кривизна которой не превышает некоторый (задаётся на усмотрение разработчика)
минимальный радиус разворота.

Выбор сложности решения оставляется на усмотрение разработчика. В зависимости от
выбранного варианта, качества реализации и наличия дополнительных свойств, не указанных
в этом требовании (например, ускорение и замедление, отработка сложных траекторий),
будет определяться конкурсное преимущество кандидата среди прочих соискателей.