package com.kolpo.vehiclemovement

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_map.*

class MapFragment : Fragment() {

    private val viewModel: MapViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.stateLiveData.observe(this, Observer {
            car_view.apply {
                x = it.vehicle.location.x.toFloat() - (car_view.width / 2)
                y = it.vehicle.location.y.toFloat() - (car_view.height / 2)
                rotation = it.vehicle.angle
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_map, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val detector = GestureDetector(
            map_container.context,
            object : GestureDetector.SimpleOnGestureListener() {
                override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
                    e?.let { viewModel.onTap(it.x, it.y) }
                    return true
                }
            }
        )
        map_container.setOnTouchListener { _, motionEvent ->
            detector.onTouchEvent(motionEvent)
            return@setOnTouchListener true
        }
    }
}