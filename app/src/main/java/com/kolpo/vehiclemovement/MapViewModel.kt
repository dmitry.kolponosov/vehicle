package com.kolpo.vehiclemovement

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.app.Application
import android.graphics.Path
import android.graphics.PathMeasure
import android.graphics.Point
import android.view.animation.LinearInterpolator
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlin.math.*

class MapViewModel(application: Application) : AndroidViewModel(application) {

    private val mStateLiveData = MutableLiveData<MapState>(MapState(Vehicle(), false))
    val stateLiveData: LiveData<MapState>
        get() = mStateLiveData

    fun onTap(x: Float, y: Float) {
        moveVehicleTo(x, y)
    }

    private fun moveVehicleTo(x: Float, y: Float) {
        val state = mStateLiveData.value ?: return
        if (state.animationInProgress) return

        val toPoint = Point(round(x).toInt(), round(y).toInt())
        getSimpleAnimator(state.vehicle.location, toPoint, state.vehicle.angle).apply {
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    updateAnimationProgress(false)
                }
            })
        }.start()
    }

    private fun getSimpleAnimator(fromPoint: Point, toPoint: Point, fromAngle: Float): Animator {
        val toAngle = Math.toDegrees(
            atan2((fromPoint.x - toPoint.x).toDouble(), (fromPoint.y - toPoint.y).toDouble())
        ).toFloat() * -1f

        return AnimatorSet().apply {
            playSequentially(
                getRotationAnimator(fromAngle, toAngle),
                getMovementAnimator(fromPoint, toPoint)
            )
        }
    }

    private fun getRotationAnimator(fromAngle: Float, toAngle: Float): Animator {
        var angleDiff = toAngle - fromAngle
        if (abs(angleDiff) > 180) {
            angleDiff = (360 - abs(angleDiff)) * sign(angleDiff) * -1f
        }
        return ValueAnimator.ofFloat(0F, angleDiff).apply {
            interpolator = LinearInterpolator()
            duration = (abs(angleDiff) / ROTATING_SPEED * 1000f).roundToLong()
            addUpdateListener { valueAnimator ->
                var angle = fromAngle + valueAnimator.animatedValue as Float
                if (abs(angle) > 180) {
                    angle = (360 - abs(angle)) * sign(angle) * -1f
                }
                updateVehicle(angle)
            }
        }
    }

    private fun getMovementAnimator(fromPoint: Point, toPoint: Point): Animator {
        val path = Path().apply {
            moveTo(fromPoint.x.toFloat(), fromPoint.y.toFloat())
            lineTo(toPoint.x.toFloat(), toPoint.y.toFloat())
        }
        val pathMeasure = PathMeasure(path, false)
        val pathLength = pathMeasure.length

        return ValueAnimator.ofFloat(0.0f, 1.0f).apply {
            interpolator = LinearInterpolator()
            duration = (pathLength / MOVEMENT_SPEED * 1000f).roundToLong()
            addUpdateListener(object : ValueAnimator.AnimatorUpdateListener {
                private val point = FloatArray(2)
                override fun onAnimationUpdate(animation: ValueAnimator) {
                    pathMeasure.getPosTan(
                        pathMeasure.length * animation.animatedFraction,
                        point, null
                    )
                    updateVehicle(Point(point[0].roundToInt(), point[1].roundToInt()))
                }
            })
        }
    }

    private fun updateVehicle(angle: Float) {
        val location = mStateLiveData.value?.vehicle?.location ?: return
        val animationInProgress = mStateLiveData.value?.animationInProgress ?: return
        mStateLiveData.postValue(
            MapState(Vehicle(location, angle), animationInProgress)
        )
    }

    private fun updateVehicle(location: Point) {
        val angle = mStateLiveData.value?.vehicle?.angle ?: return
        val animationInProgress = mStateLiveData.value?.animationInProgress ?: return
        mStateLiveData.postValue(
            MapState(Vehicle(location, angle), animationInProgress)
        )
    }

    private fun updateAnimationProgress(flag: Boolean) {
        val location = mStateLiveData.value?.vehicle?.location ?: return
        val angle = mStateLiveData.value?.vehicle?.angle ?: return
        mStateLiveData.postValue(
            MapState(Vehicle(location, angle), flag)
        )
    }

    companion object {
        const val ROTATING_SPEED = 200F
        const val MOVEMENT_SPEED = 1000F
    }

    data class MapState(
        val vehicle: Vehicle,
        val animationInProgress: Boolean
    )
}