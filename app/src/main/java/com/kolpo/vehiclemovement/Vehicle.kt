package com.kolpo.vehiclemovement

import android.graphics.Point

data class Vehicle(
    var location: Point = Point(0, 0),
    var angle: Float = 0F
)